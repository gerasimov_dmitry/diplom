package ru.omsu.imit.gerasimov.restful.mappers;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ru.omsu.imit.gerasimov.restful.exceptions.ListException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;
import ru.omsu.imit.gerasimov.restful.utils.ClassInstances;


@Provider
public class WrongURLExceptionMapper implements	ExceptionMapper<NotFoundException> {

    @Override
	public Response toResponse(NotFoundException exception) {
		return ClassInstances.failureResponse(Status.NOT_FOUND, new ListException(ErrorCode.WRONG_URL));
	}
}