package ru.omsu.imit.gerasimov.restful.dao.mysql;

import java.util.List;
import java.util.Set;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import ru.omsu.imit.gerasimov.restful.dao.DAO;

import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;
import ru.omsu.imit.gerasimov.restful.hibernate.hibernateSessionFactory.HibernateSessionFactory;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

public class MySQL implements DAO{

    private Session session;   
    
    @Override
    public void save(Object obj)
    {
        session = HibernateSessionFactory.getSessionFactory().getCurrentSession();
        session.getTransaction().begin();
        session.update(obj);
        session.flush();
        session.getTransaction().commit();
        session.close();
    }
    
    @Override
    public Object addItem(Object obj) {
        session = HibernateSessionFactory.getSessionFactory().getCurrentSession();
        session.getTransaction().begin();
        session.saveOrUpdate(obj);
        session.getTransaction().commit();
        session.close();
        return obj;
    }
    
    @Override
    public <T> T getItemById(int id, Class<T> clazz) {
        session = HibernateSessionFactory.getSessionFactory().getCurrentSession();
        session.getTransaction().begin();
        T obj = (T)session.get(clazz, id);
        session.getTransaction().commit();
        session.close();
        Hibernate.initialize(obj);
        return obj;
    }

    @Override
    public <T> List<T> getAll(Class<T> clazz) {
        session = HibernateSessionFactory.getSessionFactory().getCurrentSession();
        Criteria criteria;
        session.getTransaction().begin();
        criteria = session.createCriteria(clazz);
        List<T> items = criteria.list();
        session.getTransaction().commit();
        session.close();
        return items;
        
    }


    @Override
    public <T> void deleteById(int id, Class <T> clazz) {
        Object t = this.getItemById(id, clazz);
        session = HibernateSessionFactory.getSessionFactory().getCurrentSession();
        session.getTransaction().begin();
        session.delete(t);
        session.getTransaction().commit();
        session.close();
       
    }

    @Override
    public <T> T editById(int id, Object obj, Class<?> clazz) throws BaseException {
        session = HibernateSessionFactory.getSessionFactory().getCurrentSession();
        if(!clazz.isInstance(obj))
            throw new BaseException(ErrorCode.UNCORRECT_VALUE, "obj is not an instance of " + clazz.getName());
        T t = (T)this.getItemById(id, clazz);
        if(!(t.equals(obj)))
        {
            session.getTransaction().begin();
            session.saveOrUpdate(t);
            session.getTransaction().commit();
            session.close();
        }
        return t;
    }
    
    @Override
    public <T> List<T> getItemsByQueryParam(Set<String> keys, Map<String, String> params, Class<T> clazz)
    {
        session = HibernateSessionFactory.getSessionFactory().getCurrentSession();
        Criteria criteria;
        session.getTransaction().begin();
        criteria = session.createCriteria(clazz);
        
        /*keys.forEach((key) -> {
            if(params.get(key) != null)
                criteria.add(Restrictions.like(key, "%" + params.get(key) + "%"));
        });*/
        for(String key: keys)
            if(params.get(key) != null && !params.get(key).isEmpty())
            {
                //flag = true;
                criteria.add(Restrictions.like(key, "%" + params.get(key) + "%"));
            }
        List<T> items = criteria.list();
        session.getTransaction().commit();
        session.close();
        return items;
    }
    
    
}
