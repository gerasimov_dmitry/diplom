package ru.omsu.imit.gerasimov.restful.resources;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

public interface Resource {
    
    public Response addItem(String json);
    public Response getItemById(@PathParam(value = "id") int id);
    public Response getAllItems();
    public Response deleteById(@PathParam(value = "id") int id);
    public Response editById(@PathParam(value = "id") int id, String json);
    
}
