package ru.omsu.imit.gerasimov.restful.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.omsu.imit.gerasimov.restful.dao.DAO;
import ru.omsu.imit.gerasimov.restful.dao.mysql.MySQL;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;
import ru.omsu.imit.gerasimov.restful.response.BaseResponse;
import ru.omsu.imit.gerasimov.restful.utils.ClassInstances;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;
import ru.omsu.imit.gerasimov.restful.dto.*;
import ru.omsu.imit.gerasimov.restful.dto.person.Author;


public class ServiceControl implements Service{

    private ObjectMapper om = new ObjectMapper();//.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    private static DAO mysql = new MySQL();
    
    @Override
    public <T> Response addItem(String json, Class<T> clazz) {
        try {

            T a = om.readValue(json, clazz);
            mysql.addItem(a);
            String response = om.writeValueAsString(a);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (IOException ex) {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.UNKNOWN_ERROR, ex.getMessage()));
        }
    }
    
    @Override
    public <T> Response getItemById(int id, Class<T> clazz) {
        T a = (T)mysql.getItemById(id, clazz);
        try {
            String perttyStr = om.writeValueAsString(a);
            return Response.ok(perttyStr, MediaType.APPLICATION_JSON).build();
        } catch (JsonProcessingException ex) {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.UNKNOWN_ERROR, ex.getMessage()));
        }
    }

    @Override
    public <T> Response deleteById(int id, Class<T> clazz) {
        mysql.deleteById(id, clazz);
        try {
            return Response.ok(om.writeValueAsString(new BaseResponse()), MediaType.APPLICATION_JSON).build();
        } catch (JsonProcessingException ex) {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.UNKNOWN_ERROR, ex.getMessage()));
        }
    }

    @Override
    public <T> Response editById(int id, String json, Class<T> clazz) {        
        T a = (T)mysql.getItemById(id, clazz);
        T b;
        try {
            b = om.readValue(json, clazz);
        } catch (IOException ex) {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.UNKNOWN_ERROR, ex.getMessage()));
        }
        
        if(a instanceof Author)
            ((Author) a).copy((Author)b);
        else if(a instanceof Product)
            ((Product)a).copy((Product)b);
        else if(a instanceof Genre)
            ((Genre)a).copy((Genre)b);
        else if(a instanceof Instance)
            ((Instance)a).copy((Instance)b);
        else if(a instanceof Section)
            ((Section)a).copy((Section)b);
        else if(a instanceof Type)
            ((Type)a).copy((Type)b);
        mysql.save(a);
        String response;
        try
        {
            response = om.writeValueAsString(a);
        }catch(IOException ex)
        {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.JSON_PARSE_EXCEPTION, ex.getMessage()));
        }
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    @Override
    public <T> Response getAllItems(Class<T> clazz) {
        List<T> items = mysql.getAll(clazz);
        String response;
        try
        {
            response = om.writeValueAsString(items);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }catch(JsonProcessingException ex)
       {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.UNKNOWN_ERROR, ex.getMessage()));
        }
    }
    
    @Override
    public <T> Response getItemByQueryParam(Set<String> keys, Map<String, String> params, Class<T> clazz)
    {
        List<T> items = mysql.getItemsByQueryParam(keys, params, clazz);
        String response;
        try
        {
            response = om.writeValueAsString(items);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }catch(JsonProcessingException ex)
        {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.UNKNOWN_ERROR, ex.getMessage()));
        }
    }
    
}
