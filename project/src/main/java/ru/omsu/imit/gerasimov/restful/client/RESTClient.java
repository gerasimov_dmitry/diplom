package ru.omsu.imit.gerasimov.restful.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.ws.rs.client.Client;

//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
import java.io.IOException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.omsu.imit.gerasimov.restful.response.FailureResponse;



public class RESTClient {
    
private static final ObjectMapper om = new ObjectMapper();

        private static Client RESTClient()
        {
            return ClientBuilder.newClient();
        }


	public Object get(String url, Class<?> classResponse) {
		javax.ws.rs.client.Client client = RESTClient();
		WebTarget myResource = client.target(url);
		Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
		Response response = (Response)builder.get();
		String body = response.readEntity(String.class);
		int httpCode = response.getStatus();
		Object obj;
                try
                {
		if(httpCode == Response.Status.OK.getStatusCode()) 
			obj = om.readValue(body, classResponse);
			else {
				obj = om.readValue(body, FailureResponse.class);
			}
                }catch(IOException ex)
                {
                    obj = ex.getMessage();
                }
		client.close();
		return obj;
	}
        
	
	public Object post(String url, Object object, Class<?> classResponse) {
		javax.ws.rs.client.Client client = RESTClient();
		WebTarget myResource = client.target(url);
		Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
                Entity e = Entity.json(object);
		Response response = builder.post(e);
		String body = response.readEntity(String.class);
		int httpCode = response.getStatus();
		Object obj;   
                try
                {
		if(httpCode == Response.Status.OK.getStatusCode())                        
                            obj = om.readValue(body, classResponse);
		else
			obj = om.readValue(body, FailureResponse.class);
                }catch(IOException ex)
                {
                    obj = null;
                }
		client.close();
		return obj;       
	}

	public Object postWrongJson(String url, String json, Class<?> classResponse) {
		javax.ws.rs.client.Client client = RESTClient();
		WebTarget myResource = client.target(url);
		Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
		Response response = builder.post(Entity.json(json));
		String body = response.readEntity(String.class);
                Object obj;
                try
                {
                    obj = om.readValue(body, FailureResponse.class);
                }catch(IOException ex)
                {
                    obj = null;
                }
		client.close();
		return obj;
	}

	public Object put(String url, Object object,	Class<?> classResponse) {
		javax.ws.rs.client.Client client = RESTClient();
		WebTarget myResource = client.target(url);
		Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
		Response response = builder.put(Entity.json(object));

		String body = response.readEntity(String.class);
		int httpCode = response.getStatus();
		Object obj;
                try
                {
                    if(httpCode == Response.Status.OK.getStatusCode())  
                            obj = om.readValue(body, classResponse);
                    else 
                            obj = om.readValue(body, FailureResponse.class);
                }catch(IOException ex)
                {
                    obj = null;
                }
		client.close();
		return obj;
	}

	public Object delete(String url, Class<?> classResponse) {
		javax.ws.rs.client.Client client = RESTClient();
		WebTarget myResource = client.target(url);
		Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
		Response response = (Response) builder.delete();
		String body = response.readEntity(String.class);
		int httpCode = response.getStatus();
		Object obj;
                try
                {
                    if(httpCode == Response.Status.OK.getStatusCode()) 
                        obj = om.readValue(body, classResponse);
                    else
                        obj = om.readValue(body, FailureResponse.class);
                }catch(IOException ex)
                {
                    obj = null;
                }
		client.close();
		return obj;
	}    
    
}
