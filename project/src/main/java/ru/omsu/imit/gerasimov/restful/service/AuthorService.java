package ru.omsu.imit.gerasimov.restful.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.omsu.imit.gerasimov.restful.dao.DAO;
import ru.omsu.imit.gerasimov.restful.dao.mysql.MySQL;
import ru.omsu.imit.gerasimov.restful.dto.person.Author;
import ru.omsu.imit.gerasimov.restful.dto.Product;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;
import ru.omsu.imit.gerasimov.restful.utils.ClassInstances;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

public class AuthorService {
    
    private ObjectMapper om = new ObjectMapper();
    private static DAO mysql = new MySQL();
    
    public Response getAuthorsProducts(int id)
    {
        Author author = (Author)mysql.getItemById(id, Author.class);
        String response;
        try
        {
            response = om.writeValueAsString(author.getProducts());
        }catch(JsonProcessingException ex)
        {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.JSON_PARSE_EXCEPTION, ex.getMessage()));
        }
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
    
    
    public Response addProductToAuthor(int id, String json)
    {
        Product product;
        Author author = (Author)mysql.getItemById(id, Author.class);
        String response;
        try {
            product = om.readValue(json, Product.class);
            author.addProduct(product);
            product.addAuthor(author);
            response = om.writeValueAsString(author);
        } catch (IOException | BaseException ex) {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.JSON_PARSE_EXCEPTION, ex.getMessage()));
        }
        mysql.save(author);
                
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
    
    public Response setProductToAuthor(int authorId, int productId)
    {
        Author author = (Author)mysql.getItemById(authorId, Author.class);
        Product product = (Product)mysql.getItemById(productId, Product.class);
        String response;
        try
        {
            author.addProduct(product);
            product.addAuthor(author);
        }catch(BaseException ex)
        {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.ITEM_NOT_FOUND, ex.getMessage()));
        }
        mysql.save(author);
        try
        {
            response = om.writeValueAsString(author);
        }catch(IOException ex)
        {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.JSON_PARSE_EXCEPTION, ex.getMessage()));
        }
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
    
    public Response resetProductToAuthor(int authorId, int productId)
    {
        Author author = (Author)mysql.getItemById(authorId, Author.class);
        Product product = (Product)mysql.getItemById(productId, Product.class);
        String response;
        try
        {
            author.deleteProduct(product);
            product.deleteAuthor(author);
        }catch(BaseException ex)
        {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.ITEM_NOT_FOUND, ex.getMessage()));
        }
        mysql.save(author);
        mysql.save(product);
        try
        {
            response = om.writeValueAsString(author);
        }catch(IOException ex)
        {
            return ClassInstances.failureResponse(new BaseException(ErrorCode.JSON_PARSE_EXCEPTION, ex.getMessage()));
        }
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
}
