package ru.omsu.imit.gerasimov.restful.dto.person;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

@Entity(name = "Client")
@Table(name = "clients")
@AttributeOverride(name = "id", column = @Column(name = "ClientID"))
public class Client extends Person{
    @Column(name = "Address")
    private String address;
    @Column(name = "Email")
    private String email;
    @Column(name = "Phone")
    private String phone;
    
    public Client(String firstName, String lastName, String address, String email, String phone) throws StringException
    {
        super(firstName, lastName);
        this.setAddress(address);
        this.setEmail(email);
        this.setPhone(phone);
    }
    
    public Client(Client client) throws StringException
    {
        this.setAddress(client.getAddress());
        this.setEmail(client.getEmail());
        this.setFirstName(client.getFirstName());
        this.setLastName(client.getLastName());
        this.setPhone(client.getPhone());
    }
    
    public Client(String firstName, String lastName, String address, String phone) throws StringException
    {
        this(firstName, lastName, address, null, phone);
    }
    
    public Client()
    {
        super();
    }
    
    public void edit(Client client)
    {
        super.copy(client);
        this.address = client.getAddress();
        this.email = client.getEmail();
        this.phone = client.getPhone();
    }
    
    public final void setAddress(String address) throws StringException
    {
        if(address == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(address))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.address = address;
    }
    
    public final void setEmail(String email) throws StringException
    {
        this.email = email;
    }
    
    public final void setPhone(String phone) throws StringException
    {
        if(phone == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(phone))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.phone = phone;
    }
    
    public final String getAddress()
    {
        return this.address;
    }
    
    public final String getEmail()
    {
        return this.email;
    }
    
    public final String getPhone()
    {
        return this.phone;
    }
}
