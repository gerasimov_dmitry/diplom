package ru.omsu.imit.gerasimov.restful.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.apache.commons.lang.StringUtils;

import ru.omsu.imit.gerasimov.restful.dto.person.Client;
import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

@Entity(name = "Order")
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OrderID")
    private int id;
    @Column(name = "Data_begin")
    private String dateBegin;
    @Column(name = "Data_end")
    private String dateEnd;
    @Column(name = "Sum")
    private float sum;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ClientID")
    private Client client;
    
    private void countSumm()
    {
        //Код подсчета суммы всех элементов заказа.
    }
    
    //Добавить "экземпляры" в конструктор!!!
    public Order(String dateBegin, String dateEnd, Client client) throws StringException
    {
        this.setDateBegin(dateBegin);
        this.setDateEnd(dateEnd);
        this.setClient(client);
    }
    
    public final void setClient(Client client)
    {
        this.client = client;
    }
    
    public final Client getClient()
    {
        return this.client;
    }
    
    public final void setDateBegin(String dateBegin) throws StringException
    {
        if(dateBegin == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(dateBegin))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.dateBegin = dateBegin;
    }
    
    public final void setDateEnd(String dateEnd) throws StringException, StringException
    {
        if(dateEnd == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(dateEnd))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.dateEnd = dateEnd;
    }
    
    public final int getId()
    {
        return this.id;
    }
    
    public final String getDateBegin()
    {
        return this.dateBegin;
    }
    
    public final String getDateEnd()
    {
        return this.dateEnd;
    }
    
    public final float getSum()
    {
        return this.sum;
    }
}
