package ru.omsu.imit.gerasimov.restful.dao.mysql.config;

public final class Config {
    private static final int mySQLPort = 3306;
    private static final String databaseName = "mydb";
    private static final String mySQLDatabaseUser = "root";
    private static final String mySQLDatabasePassword = "qwertyO1";
    private static final String anotherParameters = "autoReconnect=true&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC\";";
    
    public final static int getMySQLPort()
    {
        return mySQLPort;
    }
    
    public final static String getDatabaseName()
    {
        return databaseName;
    }
    
    public final static String getMySQLDatabaseUser()
    {
        return mySQLDatabaseUser;
    }
    
    public final static String getMySQLDatabasePassword()
    {
        return mySQLDatabasePassword;
    }
    
    public final static String getAnotherParameters()
    {
        return anotherParameters;
    }
}
