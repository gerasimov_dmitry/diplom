package ru.omsu.imit.gerasimov.restful.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

import org.apache.commons.lang.StringUtils;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.omsu.imit.gerasimov.restful.dto.person.Author;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;

@Entity(name = "Product")
@Table(name = "product")
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property="@UUID")
public class Product implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ProductID")
    private int id;
    @Column(name = "Title")
    private String title;
    @Column(name = "Description")
    private String description;
    @Column(name = "Keywords")
    private String keywords;
    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "authors_and_products",
    joinColumns = @JoinColumn(name = "ProductID"),
    inverseJoinColumns = @JoinColumn(name = "AuthorID"))
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Author> authors;
    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinTable(name = "products_and_genres",
    joinColumns = @JoinColumn(name = "ProductID"),
    inverseJoinColumns = @JoinColumn(name = "GenreID"))
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Genre> genres;
    @JsonIgnore
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Instance> instances;
    
    public Product()
    {

    }
    
    public Product(String title, String description, List<String> keywords, List<Author> authors, List<Genre> genres) 
            throws StringException
    {
        this.setTitle(title);
        this.setDescription(description);
        //this.setKeywords(keywords);
        this.setAuthors(authors);
        this.setGenres(genres);
    }
    
    public Product(String title, String description, String keywords, List<Author> authors, List<Genre> genres) 
            throws StringException
    {
        this.setTitle(title);
        this.setDescription(description);
        this.setKeywords(keywords);
        this.setAuthors(authors);
        this.setGenres(genres);
    }
    
    public Product(String title, String description, String keywords, List<Author> authors) 
            throws StringException
    {
        this(title,description, keywords, authors, null);
    }
    
    public Product(String title, String description, List<String> keywords, List<Author> authors) 
            throws StringException
    {
        this(title,description, keywords, authors, null);
    }
    
    public void copy(Product other)
    {
        this.title = other.getTitle();
        this.authors = other.getAuthors();
        this.description = other.getDescription();
        this.genres = other.getGenres();
        this.instances = other.getInstances();
        this.keywords = other.getKeywords();
    }
    
    public final int addInstance(Instance instance) throws BaseException
    {
        if(instance == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(this.instances == null)
            this.instances = new ArrayList<>();
        for(Instance i : this.instances)
            if(i.getId() == instance.getId())
                return this.instances.size() - 1;
        this.instances.add(instance);
        return this.instances.size() - 1;
    }
    
    public final void deleteInstance(Instance instance) throws BaseException
    {
        if(instance == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(instance.getId() == 0)
            return;
        for(int i = 0; i < this.instances.size(); i++)
            if(instance.getId() == this.instances.get(i).getId())
                this.instances.remove(i);
    }
    
    public final void setInstance(List<Instance> instances)
    {
        /*if(instances == null || instances.isEmpty())
            throw new ListException(ErrorCode.NULL_PTR, "List of instances has a null pointer");
        */
        if(instances == null)
            this.instances = new ArrayList<>();
        else
            this.instances = instances;
    }
    
    public final List<Instance> getInstances()
    {
        return this.instances;
    }
    
    public final int addGenre(Genre genre) throws BaseException
    {
        if(genre == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Genre has a null pointer");
        if(this.genres == null)
            this.genres = new ArrayList<>();
        for(Genre g : this.genres)
            if(g.getId() == genre.getId())
                return this.genres.size() - 1;
        this.genres.add(genre);
        return this.genres.size() - 1;
    }
    
    public final void deleteGenre(Genre genre) throws BaseException
    {
        if(genre == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(genre.getId() == 0)
            return;
        for(int i = 0; i < this.genres.size(); i++)
            if(genre.getId() == this.genres.get(i).getId())
                this.genres.remove(i);
    }
    
    public final void setGenres(List<Genre> genres)
    {
        if(genres == null)
            this.genres = new ArrayList<>();
        else
            this.genres = genres;
    }
    
    public final int addAuthor(Author author) throws BaseException
    {
        if(author == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Author has a null pointer");
        if(this.authors == null)
            this.authors = new ArrayList<>();
        for(Author a : this.authors)
            if(a.getId() == author.getId())
                return this.authors.size() - 1;
        this.authors.add(author);
        return this.authors.size() - 1;
    }
    
    public final void deleteAuthor(Author author) throws BaseException
    {
        if(author == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(author.getId() == 0)
            return;
        for(int i = 0; i < this.authors.size(); i++)
            if(author.getId() == this.authors.get(i).getId())
                this.authors.remove(i);
    }
    
    public final void setAuthors(List<Author> authors)
    {
        if(authors == null)
            this.authors = new ArrayList<>();
        else
            this.authors = authors;
    }
    
    public final void setTitle(String title) throws StringException
    {
        if(title == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(title))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.title = title;
    }
    
    public final void setDescription(String description) throws StringException
    {
        if(description == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(description))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.description = description;
    }
    
    public final void setKeywords(String keywords)
    {
        //this.keywordsArray = Arrays.asList(keywords.split("[\\s,]+"));
        this.keywords = keywords;
    }
    
    /*public final void setKeywords(List<String> keywords)
    {
       // this.keywordsArray = keywords;
        StringBuilder sb = new StringBuilder();
        int localSize = keywords.size() - 1;
        for(int i = 0; i < localSize; i++)
            sb.append(keywords.get(i)).append(", ");
        sb.append(keywords.get(localSize));
        this.keywords = sb.toString();
    }*/
    
    public final List<Author> getAuthors()
    {
        return this.authors;
    }
    
    public final List<Genre> getGenres()
    {
        return this.genres;
    }
    
    public final String getTitle()
    {
        return this.title;
    }
    
    public final String getDescription()
    {
        return this.description;
    }
    
    /*public final List<String> getKeywords()
    {
        return this.keywordsArray;
    }*/
   
    public final String getKeywords()
    {
        return this.keywords;
    }
    
    public final int getId()
    {
        return this.id;
    }
    @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
            return true;
        if(getClass() != obj.getClass())
            return false;
        
        Product t = (Product)obj;
        if(this.getId() != t.getId())
            return false;
        if(!this.getTitle().equals(t.getTitle()))
            return false;
        if(!this.getDescription().equals(t.getDescription()))
            return false;
        if(!this.getKeywords().equals(t.getKeywords()))
            return false;
        return this.getAuthors() == t.getAuthors();    
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.id;
        hash = 89 * hash + Objects.hashCode(this.title);
        hash = 89 * hash + Objects.hashCode(this.description);
        hash = 89 * hash + Objects.hashCode(this.keywords);
        hash = 89 * hash + Objects.hashCode(this.authors);
        return hash;
    }
    
}
