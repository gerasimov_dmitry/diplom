package ru.omsu.imit.gerasimov.restful.server.config;

public class Settings {
    
    private static int restHttpPort = 8888;
    
    public static int getRestHttpPort()
    {
        return restHttpPort;
    }
    
}
