package ru.omsu.imit.gerasimov.restful.dao;

import java.util.List;
import java.util.Set;
import java.util.Map;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;

public interface DAO {
    
    public void save(Object obj);
    public Object addItem(Object obj);
    public <T> T getItemById(int id, Class<T> clazz);
    public <T> List<T> getAll(Class<T> clazz);
    public <T> T editById(int id, Object obj, Class<?> clazz) throws BaseException;
    public <T> void deleteById(int id, Class<T> clazz);
    public <T> List<T> getItemsByQueryParam(Set<String> keys, Map<String, String> params, Class<T> clazz);
}
