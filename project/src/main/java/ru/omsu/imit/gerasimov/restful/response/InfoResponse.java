package ru.omsu.imit.gerasimov.restful.response;

public class InfoResponse extends BaseResponse{
    
    private String text;
    private int id;
    
    public InfoResponse(int id, String text){
        super();
        this.id = id;
        this.text = text;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    public String getText()
    {
        return this.text;
    }
            
    
}
