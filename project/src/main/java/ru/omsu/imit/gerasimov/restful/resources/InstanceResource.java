package ru.omsu.imit.gerasimov.restful.resources;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.omsu.imit.gerasimov.restful.dto.Instance;
import ru.omsu.imit.gerasimov.restful.service.Service;
import ru.omsu.imit.gerasimov.restful.service.ServiceControl;

@Path("/api")
public class InstanceResource implements Resource{

    private static Class clazz = Instance.class;
    private static Service service = new ServiceControl();
    
    @Override
    @POST
    @Path("/instances")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addItem(String json) {
        return service.addItem(json, clazz);
    }

    @Override
    @GET
    @Path("/instances/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getItemById(@PathParam(value = "id")int id) {
        return service.getItemById(id, clazz);
    }

    @Override
    @GET
    @Path("/instances")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllItems() {
        return service.getAllItems(clazz);
    }
    
    @GET
    @Path("/instancesquery")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getItemByQueryParam(
            @QueryParam("count") String count       
    )
    {
        Map<String, String> params = new HashMap<>();
        params.put("count", count);
        Set<String> setKeys = params.keySet();
        return service.getItemByQueryParam(setKeys, params, clazz);
    }

    @Override
    @DELETE
    @Path("/instances/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam(value = "id")int id) {
        return service.deleteById(id, clazz);
    }

    @Override
    @PUT
    @Path("/instances/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editById(@PathParam(value = "id")int id, String json) {
        return service.editById(id, json, clazz);
    }
    
}
