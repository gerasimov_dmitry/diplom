package ru.omsu.imit.gerasimov.restful.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

@Entity(name = "Status")
@Table(name = "types")
public class Status {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "StatusID")
    private int id;
    
    @Column(name = "Status")
    private String statusName;
    
    public final void setStatusName(String statusName) throws StringException
    {
        if(statusName == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(statusName))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.statusName = statusName;
    }
    
    public final int getId()
    {
        return this.id;
    }
    
    public final String getStatusName()
    {
        return this.statusName;
    }
}
