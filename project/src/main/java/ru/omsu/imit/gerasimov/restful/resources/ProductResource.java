package ru.omsu.imit.gerasimov.restful.resources;

import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.omsu.imit.gerasimov.restful.dto.Product;
import ru.omsu.imit.gerasimov.restful.service.Service;
import ru.omsu.imit.gerasimov.restful.service.ServiceControl;

@Path("/api")
public class ProductResource{
    
    private static Service service = new ServiceControl();
    private static Class clazz = Product.class;
    
    @Path("/products")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addItem(String json) {
        return service.addItem(json, clazz);
    }

    @GET
    @Path("/products/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getItemById(@PathParam(value = "id")int id) {
        return service.getItemById(id, clazz);
    }
    
    @GET
    @Path("/productsquery")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getItemByQueryParam(
            @QueryParam("keywords") String keywords,
            @QueryParam("title") String title,
            @QueryParam("description") String description
    )
    {
        Map<String, String> params = new HashMap<>();
        params.put("keywords", keywords);
        params.put("title", title);
        params.put("description", description);
        Set<String> setKeys = params.keySet();
        return service.getItemByQueryParam(setKeys, params, clazz);
    }

    @GET
    @Path("/products")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllItems() {
        return service.getAllItems(clazz);
    }

    @DELETE
    @Path("/products/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam(value = "id") int id) {
        return service.deleteById(id, clazz);
    }

    @PUT
    @Path("/products/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editById(@PathParam(value = "id") int id, String json) {
        return service.editById(id, json, clazz);
    }
    
}
