package ru.omsu.imit.gerasimov.restful.exceptions;

import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

public class ListException extends BaseException{
    
    public ListException(ErrorCode errorCode, String errorMsg) {
        super(errorCode, errorMsg);
    }
    
    public ListException(ErrorCode errorCode)
    {
        super(errorCode);
    }
    
}
