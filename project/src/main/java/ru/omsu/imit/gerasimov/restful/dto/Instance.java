package ru.omsu.imit.gerasimov.restful.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

@Entity(name = "Instance")
@Table(name = "instances")
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property="@UUID")
public class Instance implements Serializable{
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "InstanceID")
    private int id;
    @Column(name = "Count")
    private int count;
    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "ProductID")
    private Product product;
    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "TypeID")
    private Type type;
    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinTable(name = "instances_on_sections",
    joinColumns = @JoinColumn(name = "InstanceID"),
    inverseJoinColumns = @JoinColumn(name = "SectionID"))
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Section> sections;
    
    public void copy(Instance other)
    {
        this.count = other.getCount();
        this.product = other.getProduct();
        this.sections = other.getSections();
        this.type = other.getType();
    }
    
    public final void setSections(List<Section> sections)
    {
        if(sections == null)
            this.sections = new ArrayList<>();
        else
            this.sections = sections;
    }
    
    public final int addSection(Section section) throws BaseException
    {
        if(section == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Section has a null pointer");
        if(this.sections == null)
            this.sections = new ArrayList<>();
        for(Section s : this.sections)
            if(s.getId() == section.getId())
                return this.sections.size() - 1;
        this.sections.add(section);
        return this.sections.size() - 1;
    }
    
    public final void deleteSection(Section section) throws BaseException
    {
        if(section == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(section.getId() == 0)
            return;
        for(int i = 0; i < this.sections.size(); i++)
            if(section.getId() == this.sections.get(i).getId())
                this.sections.remove(i);
    }
    
    public final List<Section> getSections()
    {
        return this.sections;
    }
    
    public final void setProduct(Product product) throws BaseException
    {
        if(product == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Product has a null pointer");
        this.product = product;
    }
    
    public final void setType(Type type) throws BaseException
    {
        if(type == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Type has a null pointer");
        this.type = type;
    }
    
    public final Product getProduct()
    {
        return this.product;
    }
    
    public final Type getType()
    {
        return this.type;
    }
    
    public final void setCount(int count) throws BaseException
    {
        if(count < 0)
            throw new BaseException(ErrorCode.UNCORRECT_VALUE);
        this.count = count;
    }
    
    public final int getCount()
    {
        return this.count;
    }
    
    public final int getId()
    {
        return this.id;
    }
}
