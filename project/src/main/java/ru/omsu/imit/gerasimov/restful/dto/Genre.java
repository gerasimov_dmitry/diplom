package ru.omsu.imit.gerasimov.restful.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;

import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

@Entity(name = "Genre")
@Table(name = "genres")
public class Genre implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "GenreID")
    private int id;
    
    @Column(name = "Genre")
    private String genreName;
    
    @JsonIgnore
    @ManyToMany(mappedBy = "genres", cascade = CascadeType.ALL, fetch = FetchType.EAGER)  
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Product> products;
    
    public Genre()
    {
    }
    
    public void copy(Genre other)
    {
        this.genreName = other.getGenreName();
        this.products = other.getProducts();
    }
    
    public final void setProducts(List<Product> products)
    {
        if(products == null)
            this.products = new ArrayList<>();
        this.products = products;
    }
    
    public final int addProduct(Product product) throws BaseException
    {
        if(product == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Product has a null pointer");
        if(this.products == null)
            this.products = new ArrayList<>();
        for(Product p: this.products)
            if(p.getId() == product.getId())
                return this.products.size() - 1;
        this.products.add(product);
        return this.products.size() - 1;
    }
    
    public final void deleteProduct(Product product) throws BaseException
    {
        if(product == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(product.getId() == 0)
            return;
        for(int i = 0; i < this.products.size(); i++)
            if(product.getId() == this.products.get(i).getId())
                this.products.remove(i);
    }
    
    public final List<Product> getProducts()
    {
        return this.products;
    }
    
    public Genre(String genreName, List<Product> products) throws StringException
    {
        this(genreName);
        this.setProducts(products);
    }
    
    public Genre(String genreName) throws StringException
    {
        this.setGenreName(genreName);
    }
    
    public final void setGenreName(String genreName) throws StringException
    {
        if(genreName == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(genreName))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.genreName = genreName;
    }
    
    public final int getId()
    {
        return this.id;
    }
    
    public final String getGenreName()
    {
        return this.genreName;
    }
}
