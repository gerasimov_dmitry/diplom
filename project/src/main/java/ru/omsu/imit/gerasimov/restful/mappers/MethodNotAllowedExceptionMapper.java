package ru.omsu.imit.gerasimov.restful.mappers;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ru.omsu.imit.gerasimov.restful.exceptions.ListException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;
import ru.omsu.imit.gerasimov.restful.utils.ClassInstances;


@Provider
public class MethodNotAllowedExceptionMapper implements	ExceptionMapper<NotAllowedException> {

    @Override
	public Response toResponse(NotAllowedException exception) {
		return ClassInstances.failureResponse(Status.METHOD_NOT_ALLOWED, new ListException(ErrorCode.METHOD_NOT_ALLOWED));
	}
}