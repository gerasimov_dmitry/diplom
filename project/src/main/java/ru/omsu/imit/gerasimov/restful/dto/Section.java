package ru.omsu.imit.gerasimov.restful.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;
import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;


@Entity(name = "Section")
@Table(name = "sections")
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property="@UUID")
public class Section implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "SectionID")
    private int id;
    
    @Column(name = "Name")
    private String sectionName;
    @JsonIgnore
    @ManyToMany(mappedBy = "sections", cascade = CascadeType.ALL, fetch = FetchType.LAZY)  
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Instance> instances;
    
    public void copy(Section other)
    {
        this.instances = other.getInstances();
        this.sectionName = other.getSectionName();
    }
    
    public Section()
    {
    }
    
    public Section(String section) throws StringException
    {
        this.setSectionName(section);
    }
    
    public final int addInstance(Instance instance) throws BaseException
    {
        if(instance == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(this.instances == null)
            this.instances = new ArrayList<>();
        for(Instance i : this.instances)
            if(i.getId() == instance.getId())
                return this.instances.size() - 1;
        this.instances.add(instance);
        return this.instances.size() - 1;
    }
    
    public final void deleteInstance(Instance instance) throws BaseException
    {
        if(instance == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(instance.getId() == 0)
            return;
        for(int i = 0; i < this.instances.size(); i++)
            if(instance.getId() == this.instances.get(i).getId())
                this.instances.remove(i);
    }
    
    public final void setInstances(List<Instance> instances)
    {
        if(instances == null)
            this.instances = new ArrayList<>();
        else
            this.instances = instances;
    }
    
    public final List<Instance> getInstances()
    {
        return this.instances;
    }
        
    public final void setSectionName(String sectionName) throws StringException
    {
        if(sectionName == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(sectionName))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.sectionName = sectionName;
    }
    
    public final int getId()
    {
        return this.id;
    }
    
    public final String getSectionName()
    {
        return this.sectionName;
    }
}
