package ru.omsu.imit.gerasimov.restful.server.config;

import org.glassfish.jersey.server.ResourceConfig;

public class ResourceConfiguration extends ResourceConfig{
    public ResourceConfiguration()
    {
        packages("ru.omsu.imit.gerasimov.restful.resources",
                "ru.omsu.imit.gerasimov.restful.mappers");
    }
    
}
