package ru.omsu.imit.gerasimov.restful.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


import ru.omsu.imit.gerasimov.restful.exceptions.ListException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;
import ru.omsu.imit.gerasimov.restful.utils.ClassInstances;


@Provider
public class JsonProcessingExceptionMapper implements
		ExceptionMapper<JsonProcessingException> {
	@Override
    public Response toResponse(JsonProcessingException exception) {
		return ClassInstances.failureResponse(Status.BAD_REQUEST, new ListException(ErrorCode.JSON_PARSE_EXCEPTION));
    }
}