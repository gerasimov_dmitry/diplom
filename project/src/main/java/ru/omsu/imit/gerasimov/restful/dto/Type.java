package ru.omsu.imit.gerasimov.restful.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;
import java.util.List;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;
import ru.omsu.imit.gerasimov.restful.exceptions.ListException;


@Entity(name = "Type")
@Table(name = "types")
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property="@UUID")
public class Type implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "TypeID")
    private int id;
    
    @Column(name = "Type")
    private String typeName;
    @JsonIgnore
    @OneToMany(mappedBy = "type", cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    private List<Instance> instances;

    public Type() {
    }
    
    public void copy(Type other)
    {
        this.instances = other.getInstances();
        this.typeName = other.getTypeName();
    }
    
    public Type(String typeName) throws StringException
    {
        this();
        this.setTypeName(typeName);
    }
    
    public final void setTypeName(String typeName) throws StringException
    {
        if(typeName == null)
            throw new StringException(ErrorCode.NULL_PTR);
        if(StringUtils.isEmpty(typeName))
            throw new StringException(ErrorCode.EMPTY_STRING);
        this.typeName = typeName;
    }
    
    public final int addInstance(Instance instance) throws BaseException
    {
        if(instance == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(this.instances == null)
            this.instances = new ArrayList<>();
        for(Instance i : this.instances)
            if(i.getId() == instance.getId())
                return this.instances.size() - 1;
        this.instances.add(instance);
        return this.instances.size() - 1;
    }
    
    public final void deleteInstance(Instance instance) throws BaseException
    {
        if(instance == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(instance.getId() == 0)
            return;
        for(int i = 0; i < this.instances.size(); i++)
            if(instance.getId() == this.instances.get(i).getId())
                this.instances.remove(i);
    }
    
    public final void setInstances(List<Instance> instances)
    {
        if(instances == null || instances.isEmpty())
            this.instances = new ArrayList<>();
        else
            this.instances = instances;
    }
    
    public final List<Instance> getInstances()
    {
        return this.instances;
    }
    
    public final int getId()
    {
        return this.id;
    }
    
    public final String getTypeName()
    {
        return this.typeName;
    }
}

