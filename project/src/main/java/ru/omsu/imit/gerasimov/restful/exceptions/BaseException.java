package ru.omsu.imit.gerasimov.restful.exceptions;

import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

     
public class BaseException extends Exception{
    
    private String errorMsg;
    private ErrorCode errorCode;
    
    public BaseException(ErrorCode errorCode, String errorMsg)
    {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }
    
    public BaseException(ErrorCode errorCode)
    {
        this(errorCode, null);
    }

    public BaseException() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public ErrorCode getErrorCode()
    {
        return this.errorCode;
    }
    
    public String getMessage()
    {
        if(this.errorMsg == null)
            return this.errorCode.getMessage();
        else
            return this.errorMsg;
    }
    
    
}
