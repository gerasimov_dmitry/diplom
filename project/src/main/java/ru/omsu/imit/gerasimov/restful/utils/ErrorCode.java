package ru.omsu.imit.gerasimov.restful.utils;

public enum ErrorCode {
    
    SUCCESS("", ""),
    ITEM_NOT_FOUND("item", "Item not found %s"),
    NULL_REQUEST("json", "Null request"),
    JSON_PARSE_EXCEPTION("json", "Json parse exception :  %s"),
    WRONG_URL("url", "Wrong URL"),
    METHOD_NOT_ALLOWED("url", "Method not allowed"),
    UNKNOWN_ERROR("error", "Unknown error"),
    EMPTY_STRING("error", "String is empty"),
    NULL_PTR("error", "null pointer exception"),
    UNCORRECT_VALUE("error", "an attempt to set a uncorrect value was found")
    ;	
    
    private String field;
    private String message;
    
    private ErrorCode(String field, String message)
    {
        this.field = field;
        this.message = message;
    }
    
    public String getMessage()
    {
        return this.message;
    }
    
    public String getField()
    {
        return this.field;
    }
    
}
