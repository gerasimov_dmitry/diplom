package ru.omsu.imit.gerasimov.restful.server;

import java.net.URI;
import javax.ws.rs.core.UriBuilder;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import ru.omsu.imit.gerasimov.restful.server.config.ResourceConfiguration;

import ru.omsu.imit.gerasimov.restful.server.config.Settings;

import ru.omsu.imit.gerasimov.restful.dao.mysql.MySQL;
import ru.omsu.imit.gerasimov.restful.dto.person.Author;

public class Server {
    
    private static org.eclipse.jetty.server.Server jettyServer;
    
    private MySQL mysql = new MySQL();
    
    private static void attachShutdownHook()
    {
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                stopServer();
            }
        });
        
    }
      
    
    public static void createServer()
    {
        URI baseUri = UriBuilder.fromUri("http://127.0.0.1").port(Settings.getRestHttpPort()).build();
        ResourceConfiguration config = new ResourceConfiguration();
        jettyServer = JettyHttpContainerFactory.createServer(baseUri, config);
        System.out.println("Starting server... ");
    }
    
    public static void stopServer()
    {
        System.out.println("Stop server...");
        try {
            jettyServer.stop();
            jettyServer.destroy();
        } catch (Exception ex) {
            System.out.println("Server stops with exception " + ex.getMessage());
            System.exit(1);
        }
    }
    
    public static void main(String[] args)
    {
        attachShutdownHook();
        createServer();
        //stopServer();
    }
            
    
    
}
