package ru.omsu.imit.gerasimov.restful.dto.person;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang.StringUtils;

import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;



@MappedSuperclass
public abstract class Person {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    
    @Column(name = "FirstName")
    private String firstName;
    
    @Column(name = "LastName")
    private String lastName;
    
    public Person(String firstName, String lastName) throws StringException
    {
        this.setFirstName(firstName);
        this.setLastName(lastName);
    }
    
    public Person()
    {
        
    }
    
    public void copy(Person person)
    {
        this.firstName = person.getFirstName();
        this.lastName = person.getLastName();
    }
    
    public final void setFirstName(String firstName) throws StringException
    {
        if(firstName == null)
            throw new StringException(ErrorCode.EMPTY_STRING, firstName);
        if(StringUtils.isEmpty(firstName))
            throw new StringException(ErrorCode.NULL_PTR);
        this.firstName = firstName;
    }
    
    public final void setLastName(String lastName) throws StringException
    {
        if(lastName == null)
            throw new StringException(ErrorCode.EMPTY_STRING, lastName);
        if(StringUtils.isEmpty(lastName))
            throw new StringException(ErrorCode.NULL_PTR);
        this.lastName = lastName;
    }
    
    public final int getId()
    {
        return this.id;
    }
    
    public final String getFirstName()
    {
        return this.firstName;
    }
    
    public final String getLastName()
    {
        return this.lastName;
    }
    
   
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        return Objects.equals(this.lastName, other.lastName);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.firstName);
        hash = 83 * hash + Objects.hashCode(this.lastName);
        return hash;
    }
}
