package ru.omsu.imit.gerasimov.restful.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import ru.omsu.imit.gerasimov.restful.response.FailureResponse;

public class ClassInstances {
    
    private static Gson GSON = new Gson();
    
    public static <T> T getClassInstanceFromJson(String json, Class<T> clazz) throws BaseException
    {
        if(StringUtils.isEmpty(json))
            throw new BaseException(ErrorCode.NULL_REQUEST);
        try
        {
            return GSON.fromJson(json, clazz);
        }catch(JsonSyntaxException ex)
        {
            throw new BaseException(ErrorCode.JSON_PARSE_EXCEPTION);
        }
    }
    
    public static Response failureResponse(Status status, BaseException exception)
    {  
        return Response.status(status).entity(GSON.toJson(new FailureResponse(exception.getErrorCode(), exception.getMessage()))).build();
    }
    
    public static Response failureResponse(BaseException exception)
    {
        return failureResponse(Status.BAD_REQUEST, exception);
    }
}
