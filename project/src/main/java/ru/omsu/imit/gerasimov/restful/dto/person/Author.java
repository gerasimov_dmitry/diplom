package ru.omsu.imit.gerasimov.restful.dto.person;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.gerasimov.restful.dto.Product;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;
import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

@Entity(name = "Author")
@Table(name = "authors")
@AttributeOverride(name = "id", column = @Column(name = "AuthorID"))
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property="@UUID")
public class Author extends Person implements Serializable{
    
    @Column(name = "Years_of_life")
    private String yearsOfLife;
    @Column(name = "MiddleName")
    private String middleName;
    @JsonIgnore
    @ManyToMany(mappedBy = "authors", cascade = CascadeType.ALL, fetch = FetchType.LAZY) 
    private List<Product> products;

    
    public Author(String firstName, String lastName, String middleName, String yearsOfLife) throws StringException
    {
        super(firstName, lastName);
        this.setMiddleName(middleName);
        this.setYearsOfLife(yearsOfLife);
        this.setProducts(null);
    } 
    
    public Author(String firstName, String lastName, String middleName, String yearsOfLife, List<Product> products) throws StringException
    {
        this(firstName, lastName, middleName, yearsOfLife);
        this.setProducts(products);
    } 

    public Author(Author author) throws StringException
    {
        this(author.getFirstName(), author.getLastName(), author.getMiddleName(), author.getYearsOfLife()/*, author.getProducts()*/);
    }
    
    public List<Product> getProducts()
    {
        return this.products;
    }
    
    public final void setProducts(List<Product> products)
    {
        if(products == null)
            this.products = new ArrayList<>();
        else
            this.products = products;
    }
    
    public final int addProduct(Product product) throws BaseException
    {
        if(this.products == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Product has a null exception");
        for(Product p : this.products)
            if(p.getId() == product.getId())
                return this.products.size() - 1;
        this.products.add(product);
        return this.products.size() - 1;
    }
    
    public final void deleteProduct(Product product) throws BaseException
    {
        if(product == null)
            throw new BaseException(ErrorCode.NULL_PTR, "Instance has a null pointer");
        if(product.getId() == 0)
            return;
        for(int i = 0; i < this.products.size(); i++)
            if(product.getId() == this.products.get(i).getId())
                this.products.remove(i);
    }
    
    public void copy(Author author)
    {
        super.copy(author);
        this.middleName = author.getMiddleName();
        this.yearsOfLife = author.getYearsOfLife();
        this.products = author.getProducts();
    }
    
    public Author()
    {
        super();
        //this.products = new ArrayList<>();
    }
    
    public final void setMiddleName(String middleName) throws StringException
    {
        if(middleName == null)
            throw new StringException(ErrorCode.EMPTY_STRING, middleName);
        if(StringUtils.isEmpty(middleName))
            throw new StringException(ErrorCode.NULL_PTR);
        this.middleName = middleName;
    }
    
    public final void setYearsOfLife(String yearsOfLife) throws StringException
    {
        if(yearsOfLife == null)
            throw new StringException(ErrorCode.EMPTY_STRING, yearsOfLife);
        if(StringUtils.isEmpty(yearsOfLife))
            throw new StringException(ErrorCode.NULL_PTR);
        this.yearsOfLife = yearsOfLife;
    }
    
    public final String getMiddleName()
    {
        return this.middleName;
    }
    
    public final String getYearsOfLife()
    {
        return this.yearsOfLife;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
            return true;
        if(getClass() != obj.getClass())
            return false;
        Author t = (Author)obj;
        if(!this.getFirstName().equals(t.getFirstName()))
            return false;
        if(!this.getLastName().equals(t.getLastName()))
            return false;
        if(!this.getMiddleName().equals(t.getMiddleName()))
            return false;
        if(!this.getYearsOfLife().equals(t.getYearsOfLife()))
            return false;
        return this.getProducts() == t.getProducts();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.yearsOfLife);
        hash = 97 * hash + Objects.hashCode(this.middleName);
        hash = 97 * hash + Objects.hashCode(this.products);
        return hash;
    }
    
}
