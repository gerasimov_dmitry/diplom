package ru.omsu.imit.gerasimov.restful.service;

import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.Response;

public interface Service {
    
    public <T> Response addItem(String json, Class<T> clazz);
    public <T> Response getItemById(int id, Class<T> clazz);
    public <T> Response deleteById(int id, Class<T> clazz);
    public <T> Response editById(int id, String json, Class<T> clazz);
    public <T> Response getAllItems(Class<T> clazz);
    public <T> Response getItemByQueryParam(Set<String> keys, Map<String, String> params, Class<T> clazz);
    
}
