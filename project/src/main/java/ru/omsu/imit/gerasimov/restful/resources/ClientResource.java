package ru.omsu.imit.gerasimov.restful.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import ru.omsu.imit.gerasimov.restful.dto.person.Client;

import ru.omsu.imit.gerasimov.restful.service.Service;

import ru.omsu.imit.gerasimov.restful.service.ServiceControl;

@Path("/api")
public class ClientResource implements Resource{
    
    private static Service service = new ServiceControl();
    private static Class clazz = Client.class;
    
    @POST
    @Path("/clients")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addItem(String json)
    {
        return service.addItem(json, clazz);
    }
    
    @GET
    @Path("/clients/{id}")
    @Produces("application/json")
    public Response getItemById(@PathParam(value = "id") int id)
    {
       return service.getItemById(id, clazz);
    }
    
    @GET
    @Path("/clients")
    @Produces("application/json")
    public Response getAllItems()
    {
        return service.getAllItems(clazz);
    }
    
    @DELETE
    @Path("/clients/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id)
    {
        return service.deleteById(id, clazz);
    }
    
    @PUT
    @Path("/clients/{id}")
    @Produces("application/json")
    @Consumes("application/json")
    public Response editById(@PathParam(value = "id") int id, String json)
    {
        return service.editById(id, json, clazz);
    }
  
    
}
