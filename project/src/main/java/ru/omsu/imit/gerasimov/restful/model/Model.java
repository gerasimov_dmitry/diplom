package ru.omsu.imit.gerasimov.restful.model;

public class Model {
    
    private int id;
    private String text;
    
    public Model(int id, String text)
    {
        this.setId(id);
        this.setText(text);
    }
    
    public Model(String text)
    {
        this(0, text);
    }
    
    public final void setId(int id)
    {
        this.id = id;
    }
    
    public final void setText(String text)
    {
        this.text = text;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    public String getText()
    {
        return this.text;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if(this == other)
            return true;
        if(other == null)
            return false;
        if(this.getClass() != other.getClass())
            return false;
        Model t = (Model)other;
        if(this.getId() != t.getId())
            return false;
        if((this.getText() == null) && (t.getText() != null))
            return false;
        if(!this.getText().equals(t.getText()))
            return false;
        return true;
    }
}
