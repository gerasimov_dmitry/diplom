package ru.omsu.imit.gerasimov.restful.response;

import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

public class FailureResponse extends BaseResponse{
    private ErrorCode errorCode;
    private String message;

    public FailureResponse(ErrorCode errorCode, String message) {
            super();
            this.errorCode = errorCode;
            this.message = message;
    }

    public FailureResponse(ErrorCode errorCode) {
            this(errorCode, "");
    }
    public ErrorCode getErrorCode() {
            return errorCode;
    }


    public String getMessage() {
            return message;
    }
}
