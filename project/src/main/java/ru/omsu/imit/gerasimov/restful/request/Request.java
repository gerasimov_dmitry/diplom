package ru.omsu.imit.gerasimov.restful.request;

public class Request {
    private String request;
    
    public Request(String request)
    {
        this.request = request;
    }
    
    public String getText()
    {
        return this.request;
    }
}
