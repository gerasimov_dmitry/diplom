package ru.omsu.imit.gerasimov.restful.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import javax.ws.rs.core.Response;
import ru.omsu.imit.gerasimov.restful.exceptions.ListException;

import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.gerasimov.restful.response.FailureResponse;

public class ListUtils {

    private static final Gson GSON = new GsonBuilder().create();
    
    public static <T> T getClassInstanceFromJson(Gson gson, String json, Class<T> clazz) throws ListException {
        if(StringUtils.isEmpty(json))
            throw new ListException(ErrorCode.NULL_REQUEST);
        try {
                return gson.fromJson(json, clazz);
        } catch (JsonSyntaxException ex) {
                throw new ListException(ErrorCode.JSON_PARSE_EXCEPTION, json);
        }
    }

    public static Response failureResponse(Response.Status status, ListException ex) {
            return Response.status(status).entity(GSON.toJson(new FailureResponse(ex.getErrorCode(), ex.getMessage()))).build();
    }

    public static Response failureResponse(ListException ex) {
            return failureResponse(Response.Status.BAD_REQUEST, ex);
    }
}
