package ru.omsu.imit.gerasimov.restful.exceptions;

import ru.omsu.imit.gerasimov.restful.utils.ErrorCode;

public class StringException extends BaseException{
    
    public StringException(ErrorCode errorCode, String errorMsg) {
        super(errorCode, errorMsg);
    }

    public StringException(ErrorCode errorCode) {
        super(errorCode);
    }
    
    
}
