package ru.omsu.imit.gerasimov.restful.resources;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.omsu.imit.gerasimov.restful.dto.Section;
import ru.omsu.imit.gerasimov.restful.service.Service;
import ru.omsu.imit.gerasimov.restful.service.ServiceControl;

@Path("/api")
public class SectionResource implements Resource{

    private static Class clazz = Section.class;
    private static Service service = new ServiceControl();
    
    @Override
    @POST
    @Path("/sections")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addItem(String json) {
        return service.addItem(json, clazz);
    }

    @Override
    @GET
    @Path("/sections/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getItemById(@PathParam(value = "id")int id) {
        return service.getItemById(id, clazz);
    }
    
    @GET
    @Path("/sectionsquery")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getItemByQueryParam(
            @QueryParam("name") String name       
    )
    {
        Map<String, String> params = new HashMap<>();
        params.put("name", name);
        Set<String> setKeys = params.keySet();
        return service.getItemByQueryParam(setKeys, params, clazz);
    }

    @Override
    @GET
    @Path("/sections")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllItems() {
        return service.getAllItems(clazz);
    }

    @Override
    @DELETE
    @Path("/sections/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam(value = "id")int id) {
        return service.deleteById(id, clazz);
    }

    @Override
    @PUT
    @Path("/sections/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editById(@PathParam(value = "id")int id, String json) {
        return service.editById(id, json, clazz);
    }
    
}
