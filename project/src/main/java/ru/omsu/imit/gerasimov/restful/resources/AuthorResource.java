package ru.omsu.imit.gerasimov.restful.resources;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.omsu.imit.gerasimov.restful.dto.person.Author;
import ru.omsu.imit.gerasimov.restful.service.AuthorService;
import ru.omsu.imit.gerasimov.restful.service.ServiceControl;
import ru.omsu.imit.gerasimov.restful.service.Service;

@Path("/api")
public class AuthorResource implements Resource{

    private static Service service = new ServiceControl();
    private static Class clazz = Author.class;
    private static AuthorService as = new AuthorService();
    
    @Override
    @POST
    @Path("/authors")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addItem(String json) {
        return service.addItem(json, clazz);
    }
    
    @Override
    @GET
    @Path("/authors/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getItemById(@PathParam(value = "id")int id) {
        return service.getItemById(id, clazz);
    }
    
    @POST
    @Path("/authors/{id}/products")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addProductToAuthor(@PathParam(value = "id") int id, String json)
    {
        return as.addProductToAuthor(id, json);
    }
    
    @GET
    @Path("/authors/{authorId}/products/{productId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response setProductToAuthor(@PathParam(value = "authorId") int authorId, @PathParam(value = "productId") int productId)
    {
        return as.setProductToAuthor(authorId, productId);
    }
    @GET
    @Path("/authors/{id}/products")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAuthorsBooks(@PathParam(value = "id") int id)
    {
        return as.getAuthorsProducts(id);
    }
    
    @GET
    @Path("/authors/{authorId}/products/{productId}/reset")
    public Response resetProductToAuthor(@PathParam(value = "authorId") int authorId, @PathParam(value = "productId") int productId)
    {
        return as.resetProductToAuthor(authorId, productId);
    }
    
    @GET
    @Path("/authorsquery")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getItemByQueryParam(
            @QueryParam("firstName") String firstName,
            @QueryParam("lastName") String lastName,
            @QueryParam("yearsOfLife") String yearsOfLife
    )
    {
        Map<String, String> params = new HashMap<>();
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("yearsOfLife", yearsOfLife);
        Set<String> setKeys = params.keySet();
        return service.getItemByQueryParam(setKeys, params, clazz);
    }
    
    @Override
    @GET
    @Path("/authors")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllItems() {
        return service.getAllItems(clazz);
    }

    @Override
    @DELETE
    @Path("/authors/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam(value = "id") int id) {
        return service.deleteById(id, clazz);
    }

    @Override
    @PUT
    @Path("/authors/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editById(@PathParam(value = "id") int id, String json) {
        return service.editById(id, json, clazz);
    }
}
