package ru.omsu.imit.gerasimov.restful.tests.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.omsu.imit.gerasimov.restful.client.RESTClient;
import ru.omsu.imit.gerasimov.restful.dto.*;
import ru.omsu.imit.gerasimov.restful.dto.person.Author;
import ru.omsu.imit.gerasimov.restful.exceptions.BaseException;
import ru.omsu.imit.gerasimov.restful.exceptions.StringException;
import ru.omsu.imit.gerasimov.restful.server.Server;
import ru.omsu.imit.gerasimov.restful.server.config.Settings;

public class CientTest {
    
    private static RESTClient client = new RESTClient();
    private static String baseUri;
    
    private static void initialize()
    {
        String hostname = null;
        /*try
        {
            hostname = InetAddress.getLocalHost().getCanonicalHostName();
        }catch(UnknownHostException ex)
        {
            System.out.println("Exception with initialize client " + ex.getMessage());
            hostname = "localhost";
        }*/
        hostname = "localhost";
        baseUri = "http://" + hostname + ":" + Settings.getRestHttpPort() + "/api";
    }
    
    public static String getBaseURI()
    {
        return baseUri;
    }
    
    @BeforeClass
    public static void startServer()
    {
        initialize();
        Server.createServer();
    }
    
    @AfterClass
    public static void stopServer()
    {
        Server.stopServer();      
    }
    /*
    @Test
    public void addAuthorTest() throws StringException
    {
        Author pushkin = new Author("Alexander", "Pushkin", "Sergeevich", "123-124");
        Author response =(Author) client.post(getBaseURI() + "/authors", pushkin, Author.class);
        assertEquals(response.getFirstName(), pushkin.getFirstName());
        assertEquals(response.getLastName(), pushkin.getLastName());
        assertEquals(response.getMiddleName(), pushkin.getMiddleName());
        assertEquals(response.getYearsOfLife(), pushkin.getYearsOfLife());
        
        
        Author responseById = (Author)client.get(getBaseURI() + "/authors/" + response.getId(), Author.class);
        assertEquals(responseById.getId(), response.getId());
        
        Author deletedAuthor = (Author)client.delete(getBaseURI() + "/authors/" + response.getId(), Author.class);
        assertEquals(deletedAuthor.getFirstName(), null);
        assertEquals(deletedAuthor.getId(), 0); 
    }
    
    
    @Test
    public void addAuthorWithBooks() throws StringException, BaseException
    {
        Genre drama = new Genre("Drama");
        Genre horror = new Genre("Horror");
        Genre responseGenreDrama = (Genre)client.post(getBaseURI() + "/genres", drama, Genre.class);
        Genre responseGenreHorror = (Genre)client.post(getBaseURI() + "/genres", horror, Genre.class);
        assertEquals(drama.getGenreName(), responseGenreDrama.getGenreName());
        assertEquals(horror.getGenreName(), responseGenreHorror.getGenreName());
 
        Author my = new Author("AuthorName", "AuthorLastName", "AuthorMiddleName", "AuthorYearsOfLife");
        Product book1 = new Product();
        Product book2 = new Product();
        book1.setTitle("Book1Title");
        book2.setTitle("Book2Title");
        book1.setDescription("Book1Description");
        book2.setDescription("Book2Description");
        book1.setKeywords("Book1Keywords");
        book2.setKeywords("Book2Keywords");
        book1.addAuthor(my);
        book2.addAuthor(my);       
        book1.addGenre(responseGenreHorror);
        book1.addGenre(responseGenreDrama);
        book2.addGenre(responseGenreHorror);
        responseGenreHorror.addProduct(book1);
        responseGenreHorror.addProduct(book2);
        responseGenreDrama.addProduct(book1);
        my.addProduct(book2);
        my.addProduct(book1);
        
        Author response = (Author)client.post(getBaseURI() + "/authors", my, Author.class);
        assertEquals(my.getFirstName(), response.getFirstName());
        assertEquals(my.getLastName(), response.getLastName());
        assertEquals(response.getMiddleName(), my.getMiddleName());
        assertEquals(response.getYearsOfLife(), my.getYearsOfLife());
        assertEquals(response.getProducts().size(), my.getProducts().size());
        int size = response.getProducts().size();
        int sizeGenre = 0;
        for(int i = 0; i < size; i++)
        {
            assertEquals(response.getProducts().get(i).getTitle(), my.getProducts().get(i).getTitle());
            assertEquals(response.getProducts().get(i).getGenres().size(), my.getProducts().get(i).getGenres().size());
            sizeGenre = response.getProducts().get(i).getGenres().size();
            for(int j = 0; j < sizeGenre; j++)
                assertEquals(response.getProducts().get(i).getGenres().get(j).getGenreName(), my.getProducts().get(i).getGenres().get(j).getGenreName());
        }
        int idAuthor = response.getId();
        
        Author deletedAuthor = (Author)client.delete(getBaseURI() + "/authors/" + idAuthor, Author.class);
        assertEquals(deletedAuthor.getFirstName(), null);
        assertEquals(deletedAuthor.getId(), 0);
        
        
        Genre deletedGenreH = (Genre)client.delete(getBaseURI() + "/genres/" + responseGenreDrama.getId(), Genre.class);
        Genre deletedGenreD = (Genre)client.delete(getBaseURI() + "/genres/" + responseGenreHorror.getId(), Genre.class);
        assertEquals(deletedGenreH.getId(), 0);
        assertEquals(deletedGenreD.getId(), 0);
        assertEquals(deletedGenreH.getGenreName(), null);
        assertEquals(deletedGenreD.getGenreName(), null);
    }
    
    @Test(expected = StringException.class)
    public void StringExceptionTest() throws StringException
    {
        new Author("A1", "A2", "A3", null);
    }
    
    @Test
    public void deleteBookFromAuthor() throws StringException, BaseException
    {
        Author a = new Author("A1", "A2", "A3", "A4");
        Product p1 = new Product();
        Genre g = new Genre("genre");
        Genre responseG = (Genre)client.post(getBaseURI() + "/genres", g, Genre.class);
        assertEquals(responseG.getGenreName(), g.getGenreName());
        responseG.addProduct(p1);
        p1.addGenre(responseG);
        p1.setTitle("B1");
        p1.setDescription("B2");
        p1.setKeywords("B3");
        p1.addAuthor(a);
        a.addProduct(p1);
        
        
        
        Author response = (Author)client.post(getBaseURI() + "/authors", a, Author.class);
        assertEquals(a.getFirstName(), response.getFirstName());
        assertEquals(a.getLastName(), response.getLastName());
        assertEquals(a.getMiddleName(), response.getMiddleName());
        assertEquals(a.getYearsOfLife(), response.getYearsOfLife());
        assertEquals(a.getProducts().size(), 1);
        
        Product responseP1 = (Product)client.delete(getBaseURI() + "/products/" + response.getProducts().get(0).getId(), Product.class);
        assertEquals(responseP1.getId(), 0);
        assertEquals(responseP1.getTitle(), null);
        
        Genre deletedGenre = (Genre)client.delete(getBaseURI() + "/genres/" + responseG.getId(), Genre.class);
        assertEquals(deletedGenre.getId(), 0);
        assertEquals(deletedGenre.getGenreName(), null);
        
    }
    
    @Test
    public void addTypeTest() throws StringException
    {
        Type aBook = new Type("book");
        Type response = (Type)client.post(getBaseURI() + "/types", aBook, Type.class);
        assertEquals(response.getTypeName(), aBook.getTypeName());
        assertNotEquals(response.getId(), 0);
        
    }
    
    @Test
    public void addSectionTest() throws StringException
    {
        Section section = new Section("Scient");
        Section response = (Section)client.post(getBaseURI() + "/sections", section, Section.class);
        assertEquals(response.getSectionName(), section.getSectionName());
        assertNotEquals(response.getId(), 0);
    }
    
    @Test
    public void addInstanceTest()
    {
        Author author;
        Product warAndPeace;
        Genre roman;
        Type abook;
        Instance abookWarAndPeace;
        Section romanSection;
        try
        {
            author = new Author("Лев", "Толстой", "Николаевич", "09.09.1828 - 20.11.1910");
            warAndPeace = new Product();
            warAndPeace.setTitle("Война и мир");
            warAndPeace.setDescription("Описание произведения \'Война и мир\'");
            warAndPeace.setKeywords("Ключевые слова произведения \'Война и мир\'");
            warAndPeace.addAuthor(author);
            author.addProduct(warAndPeace);
            
            
            roman = new Genre("Роман-эпопея");
            Genre responseRoman = (Genre)client.post(getBaseURI() + "/genres", roman, Genre.class);
            assertNotEquals(responseRoman.getId(), 0);
            
            abook = new Type("Книга");
            Type responseABook = (Type)client.post(getBaseURI() + "/types", abook, Type.class);
            assertNotEquals(responseABook.getId(), 0);
            
            responseRoman.addProduct(warAndPeace);
            warAndPeace.addGenre(responseRoman);
            
            abookWarAndPeace = new Instance();
            abookWarAndPeace.setCount(1);
            abookWarAndPeace.setType(responseABook);
            responseABook.addInstance(abookWarAndPeace);
            abookWarAndPeace.setProduct(warAndPeace);
            warAndPeace.addInstance(abookWarAndPeace);
            romanSection = new Section("Роман");
            Section responseSection = (Section)client.post(getBaseURI() + "/sections", romanSection, Section.class);
            assertNotEquals(responseSection.getId(), 0);
            
            responseSection.addInstance(abookWarAndPeace);
            abookWarAndPeace.addSection(responseSection);
            
            
        }catch(BaseException ex)
        {
            System.out.println(ex.getMessage());
            return;
        }

        Author responseProduct = (Author)client.post(getBaseURI() + "/authors", author, Author.class);
        assertNotEquals(responseProduct.getId(), 0);
       
    }

    @Test
    public void getProductByParams() 
    {
        Object response = client.get(getBaseURI() + "/productsquery?title=Война", Product[].class);
        if(response instanceof Product[])
        {
            Product[] responseProduct = (Product[])response;
            List<Product> products = Arrays.asList(responseProduct);
            assertNotEquals(products.size(), 0);
        }
    }
    
    @Test
    public void getAuthorByParams()
    {
        Object response = client.get(getBaseURI() + "/authorsquery?firstName=xan&lastName=shk", Author[].class);
        if(response instanceof Author[])
        {
            Author[] responseAuthor = (Author[])response;
            List<Author> products = Arrays.asList(responseAuthor);
            assertNotEquals(products.size(), 0);
        }
    }

    @Test
    public void editAuthorTest()
    {
        Author author = new Author();
        try {
            author.setFirstName("FN");
            author.setLastName("LN");
            author.setMiddleName("MN");
            author.setYearsOfLife("1111");
        } catch (StringException ex) {

        }
        
        Object response = client.put(getBaseURI() + "/authors/16", author, Author.class);
        if(response instanceof Author)
            assertNotEquals(((Author)response).getId(), 0);
    }

    @Test
    public void addProductToAuthorTest()
    {
        Product product = new Product();
        try
        {
            product.setTitle("TITLE");
            product.setDescription("DESCRIPTION");
            product.setKeywords("KEYWORDS");
        }catch(StringException ex)
        {
            
        }
        Object response = client.post(getBaseURI() + "/authors/16/products", product, Author.class);
        if(response instanceof Author)
            assertNotEquals(((Author)response).getId(), 0);
    }
*/
    @Test
    public void setProductToAuthorTest()
    {
        Object response = client.get(getBaseURI() + "/authors/16/products/2", Author.class);
        if(response instanceof Author)
            assertNotEquals(((Author)response).getId(), 0);
    }
}
