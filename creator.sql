-- MySQL Script generated by MySQL Workbench
-- Wed Apr 17 18:38:35 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Authors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Authors` (
  `AuthorID` INT NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(45) NOT NULL,
  `LastName` VARCHAR(45) NOT NULL,
  `Years_of_life` VARCHAR(45) NULL,
  PRIMARY KEY (`AuthorID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Product` (
  `ProductID` INT NOT NULL AUTO_INCREMENT,
  `Title` VARCHAR(45) NULL,
  `Description` VARCHAR(45) NULL,
  `Keywords` VARCHAR(125) NULL,
  PRIMARY KEY (`ProductID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Sections`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Sections` (
  `SectionID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`SectionID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Genres` (
  `GenreID` INT NOT NULL AUTO_INCREMENT,
  `Genre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`GenreID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Clients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Clients` (
  `ClientID` INT NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(45) NULL,
  `LastName` VARCHAR(45) NULL,
  `Address` VARCHAR(45) NULL,
  `Email` VARCHAR(45) NULL,
  `Phone` VARCHAR(45) NULL,
  PRIMARY KEY (`ClientID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Types` (
  `TypeID` INT NOT NULL AUTO_INCREMENT,
  `Type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`TypeID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Instances`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Instances` (
  `InstanceID` INT NOT NULL AUTO_INCREMENT,
  `Count` INT NULL,
  `ProductID` INT NOT NULL,
  `TypeID` INT NOT NULL,
  PRIMARY KEY (`InstanceID`, `ProductID`, `TypeID`),
  INDEX `fk_Instances_Items1_idx` (`ProductID` ASC) VISIBLE,
  INDEX `fk_Instances_Types1_idx` (`TypeID` ASC) VISIBLE,
  CONSTRAINT `fk_Instances_Items1`
    FOREIGN KEY (`ProductID`)
    REFERENCES `mydb`.`Product` (`ProductID`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Instances_Types1`
    FOREIGN KEY (`TypeID`)
    REFERENCES `mydb`.`Types` (`TypeID`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Product and Genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Product and Genres` (
  `ProductID` INT NOT NULL,
  `GenreID` INT NULL,
  PRIMARY KEY (`ProductID`),
  INDEX `fk_Items_has_Genres_Items1_idx` (`ProductID` ASC) VISIBLE,
  INDEX `fk_Items_has_Genres_Genres1_idx` (`GenreID` ASC) VISIBLE,
  CONSTRAINT `fk_Items_has_Genres_Items1`
    FOREIGN KEY (`ProductID`)
    REFERENCES `mydb`.`Product` (`ProductID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Items_has_Genres_Genres1`
    FOREIGN KEY (`GenreID`)
    REFERENCES `mydb`.`Genres` (`GenreID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Orders` (
  `OrderID` INT NOT NULL AUTO_INCREMENT,
  `Data_begin` DATETIME NOT NULL,
  `Data_end` DATETIME NOT NULL,
  `Sum` REAL NOT NULL,
  `ClientID` INT NULL,
  PRIMARY KEY (`OrderID`),
  INDEX `fk_Orders_Clients1_idx` (`ClientID` ASC) VISIBLE,
  CONSTRAINT `fk_Orders_Clients1`
    FOREIGN KEY (`ClientID`)
    REFERENCES `mydb`.`Clients` (`ClientID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Statuses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Statuses` (
  `StatusID` INT NOT NULL AUTO_INCREMENT,
  `Status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`StatusID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Instances of Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Instances of Order` (
  `OrderID` INT NOT NULL,
  `InstanceID` INT NULL,
  `StatusID` INT NULL,
  PRIMARY KEY (`OrderID`),
  INDEX `fk_Orders_has_Instances_Instances1_idx` (`InstanceID` ASC) VISIBLE,
  INDEX `fk_Orders_has_Instances_Orders1_idx` (`OrderID` ASC) VISIBLE,
  INDEX `fk_ItemsOfOrder_Statuses1_idx` (`StatusID` ASC) VISIBLE,
  CONSTRAINT `fk_Orders_has_Instances_Orders1`
    FOREIGN KEY (`OrderID`)
    REFERENCES `mydb`.`Orders` (`OrderID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Orders_has_Instances_Instances1`
    FOREIGN KEY (`InstanceID`)
    REFERENCES `mydb`.`Instances` (`InstanceID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ItemsOfOrder_Statuses1`
    FOREIGN KEY (`StatusID`)
    REFERENCES `mydb`.`Statuses` (`StatusID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Instances on Sections`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Instances on Sections` (
  `InstanceID` INT NOT NULL,
  `SectionID` INT NOT NULL,
  PRIMARY KEY (`InstanceID`, `SectionID`),
  INDEX `fk_Instances_has_Sections_Instances1_idx` (`InstanceID` ASC) VISIBLE,
  INDEX `fk_Instances_has_Sections_Sections1_idx` (`SectionID` ASC) VISIBLE,
  CONSTRAINT `fk_Instances_has_Sections_Instances1`
    FOREIGN KEY (`InstanceID`)
    REFERENCES `mydb`.`Instances` (`InstanceID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Instances_has_Sections_Sections1`
    FOREIGN KEY (`SectionID`)
    REFERENCES `mydb`.`Sections` (`SectionID`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Authors and their Products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Authors and their Products` (
  `AuthorID` INT NOT NULL,
  `ProductID` INT NOT NULL,
  PRIMARY KEY (`ProductID`, `AuthorID`),
  INDEX `fk_Authors_has_Items_Items1_idx` (`ProductID` ASC) VISIBLE,
  INDEX `fk_Authors_has_Items_Authors1_idx` (`AuthorID` ASC) VISIBLE,
  CONSTRAINT `fk_Authors_has_Items_Authors1`
    FOREIGN KEY (`AuthorID`)
    REFERENCES `mydb`.`Authors` (`AuthorID`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Authors_has_Items_Items1`
    FOREIGN KEY (`ProductID`)
    REFERENCES `mydb`.`Product` (`ProductID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
